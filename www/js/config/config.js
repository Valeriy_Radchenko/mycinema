var myCinema = angular.module('starter.controllers', []);

myCinema.service('config',[function() {
  var config = this;
  
  config.servicePath = 'http://www.myapifilms.com/imdb';
  config.directorPath = 'http://www.imdb.com/name/';
  
  config.CANCEL_TIMEOUT = 20000;
  config.MY_API_FILMS_TOKEN = 'fbafb7cf-61f3-4848-994c-3b08c8d88686';
  
  config.movieFrom = 1;
  config.movieTo = 20;
  
  config.cacheTimeout = 1; // in hours
  
  config.chartColors = [ '#F7464A'
                       , '#FFDA73' 
                       , '#218457'
                       , '#FF9D73'
                       , '#6C8DD5'
                       , '#BF5B30'
                       , '#FFAA00'
                       , '#BF9A30'
                       , '#70227E'
                       , '#949FB1'
                       , '#36D88E'
                       , '#BF8F30'
                       , '#FF4C00'
                       , '#5D016D'
                       , '#1142AA'
                       , '#D4CCC5'
                       , '#E2EAE9'
                       , '#FFD073'
                       , '#8F04A8'
                       , '#A66E00'
                       ];
  
}]);

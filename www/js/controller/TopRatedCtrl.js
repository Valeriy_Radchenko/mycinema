myCinema.controller('TopRatedCtrl', ['$scope', 'MovieService', function($scope, MovieService) {
  
  $scope.$on("$ionicView.beforeEnter", function(event, data) {
    $scope.noResultTitle = '';
    $scope.movies = MovieService.get();
    console.log($scope.movies);
    if (!$scope.movies || !MovieService.checkCacheTimeout()) {
      MovieService.getMoviesFromServer(function(movies) {
        $scope.movies = movies;
      });
    }
  });
  
  $scope.$on("$ionicView.afterEnter", function(event, data) {
    $scope.tabTitle = 'Top Rated Movies';
  });

  $scope.getTrailer = MovieService.getTrailer;
  $scope.showDirectorInfo = MovieService.showDirectorInfo;
  $scope.toggleFavorites = MovieService.toggleFavorites;
  $scope.join = MovieService.normalizeArray;
  $scope.showTrailer = MovieService.showTrailer;

  
  
 
  
}]);
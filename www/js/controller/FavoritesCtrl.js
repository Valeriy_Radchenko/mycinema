myCinema.controller('FavoritesCtrl', ['$scope', 'NetworkService', 'MovieService', '$ionicModal', function($scope, NetworkService, MovieService, $ionicModal) {
  
  $scope.$on("$ionicView.beforeEnter", function(event, data) {
    $scope.noResultTitle = 'Please add some movie to favorite';
    $scope.movies = MovieService.getFavorites();
  });
  
  $scope.$on("$ionicView.afterEnter", function(event, data) {
    $scope.tabTitle = 'Favorite Movies';
  });

  $scope.getTrailer = MovieService.getTrailer;
  $scope.showDirectorInfo = MovieService.showDirectorInfo;
  $scope.toggleFavorites = MovieService.toggleFavorites;
  $scope.join = MovieService.normalizeArray;
  $scope.showTrailer = MovieService.showTrailer;

  
  
  
}]);
myCinema.controller('MoviesByDecadeCtrl', ['$scope', 'MovieService', 'config', 
  function($scope, MovieService, config) {
  
  var movies = [];
  var colors = config.chartColors;
  $scope.chartOptions =  { responsive: true
                         , animateRotate : false
                         , animateScale : false
                         };
  
  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.showChart = true;
    movies = MovieService.get();
    $scope.chartData = createChartData(movies);

    console.log($scope.myData);
  });
  
  $scope.$on("$ionicView.afterLeave", function(event, data){
    // Chart error "IndexSizeError" is prevented by this action, when opening modal window. 
    $scope.showChart = false; 
  });
  
  function createChartData(movies) {
    var dates = {};
    var result = [];
    var colorIndex = 0;
    
    for (var i = 0, ii = movies.length; i < ii; i++) {
      
      var movie = movies[i];
      var year =  movie.year.slice(0,-1) + '0';
      var data = {};
      
      if (dates[year]) {
        data = dates[year];
        data.value++;
        result[data.index] = data;
      } else {
        data.year = year;
        data = { value: 1
               , color: colors[colorIndex]
               , label: year +  ' - ' + (year*1 + 9)
               , year: year
               , index: result.length
               };
        colorIndex++;
        result.push(data);
      }
      
      dates[year] = data;
    }
    
    result.sort(function(a, b) {
      if (a.year < b.year) {
        return -1;
      }
      if (a.year > b.year) {
        return 1;
      }
      return 0;
    });
    
    return result;
  }
  
}]);
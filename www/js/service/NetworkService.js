myCinema.service('NetworkService',['$http', '$httpParamSerializer', 'config',
  function($http, $httpParamSerializer, config) {
  var $network = this;
  
  $network.servicePath = config.servicePath;
  
  if (navigator.userAgent.toLocaleLowerCase().indexOf('windows') > -1 
          || navigator.userAgent.toLowerCase().indexOf('macintosh') > -1) {
    $network.servicePath = 'http://localhost/myCinema/www/proxy.php';
  }

  var CANCEL_TIMEOUT = config.CANCEL_TIMEOUT,
      MY_API_FILMS_TOKEN = config.MY_API_FILMS_TOKEN; 
  
  $network.get = function(methodName, params, callback) {
    callback = callback || function(){};
    
    var path = $network.servicePath + methodName;
    
    if (params) {
      params.token = MY_API_FILMS_TOKEN;
      path = $network.servicePath + methodName + '?' + $httpParamSerializer(params);
    }
    
    $http({ 'method': 'get'
          , 'url': path
          , timeout: CANCEL_TIMEOUT
          })

            .success(function(result, status, headers) {
              callback(true, result);
            })

            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });
          
  };
  
  $network.getWithPromise = function(methodName, params) {
    var path = $network.servicePath + methodName;
    
    if (params) {
      params.token = MY_API_FILMS_TOKEN;
      path = $network.servicePath + methodName + '?' + $httpParamSerializer(params);
    }
    
    return $http({ 'method': 'get'
                 , 'url': path
                 , timeout: CANCEL_TIMEOUT
                 });
  };
  
  $network.post = function(methodName, params, callback) {
    
    callback = callback || function(){};
    
    params = params || {};

    var path = $network.servicePath + methodName;
    
    $http({ 'method': 'post'
          , 'url': path
          , 'data':  $httpParamSerializer(params)
          , 'headers' : {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
          , timeout: CANCEL_TIMEOUT
          })
            .success(function(result, status, headers) {
              callback(true, result);
            })
            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });

  };
  
  $network.put = function(methodName, params, callback, disableDefaultErrorHandler, noToken) {
    
    callback = callback || function(){};
    
    params = params || {};

    var path = $network.servicePath + methodName;

    $http({ 'method': 'put'
          , 'url': path
          , 'data':  params
          , timeout: CANCEL_TIMEOUT
          })
            .success(function(result, status, headers) {
              callback(true, result);
            })
            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });

  };

}]);

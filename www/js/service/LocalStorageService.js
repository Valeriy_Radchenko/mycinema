myCinema.service('LocalStorageService',['$window', function($window) {
  var storage = this;
  
  var keyError = new Error('Key must be a string');
  
  storage.set = function(key, value) {
    if (typeof key !== 'string') throw keyError;
    
    if (typeof value !== 'string') {
      value = JSON.stringify(value);
    }
    
    key = 'myCinema-' + key;
    $window.localStorage.setItem(key, value);
  };
  
  storage.get = function(key) {
    if (typeof key !== 'string') throw keyError;
    
    key = 'myCinema-' + key;
    var result = $window.localStorage.getItem(key);
    if (!result) return false;
    
    try {
      result = JSON.parse(result);
    } catch (e) {}
    
    return result;
  };
  
  storage.remove = function(key) {
    if (typeof key !== 'string') throw keyError;
    
    key = 'myCinema-' + key;
    $window.localStorage.removeItem(key);
  };
  
}]);

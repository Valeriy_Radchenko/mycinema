myCinema.service('MovieService',['LocalStorageService', 'NetworkService', 'config',
  function(LocalStorageService, NetworkService, config) {
    
  var movie = this;
  var MOVIE_RATING = 'movie-rating';
  var MOVIE_RATING_CACHE_EXPIRE = 'movie-rating-cache-expire';
  var MOVIE_RATING_CACHE_CHECK_SUM = 'movie-rating-cache-check-sum';
  var MOVIE_FAVORITES = 'movie-favorites';
  var movies = LocalStorageService.get(MOVIE_RATING);
  var favorites = LocalStorageService.get(MOVIE_FAVORITES);
  var iframe = null;
  var iframeParams = {};
  
  movie.set = function(items) {
    movies = items;
    LocalStorageService.set(MOVIE_RATING, items);
    var cacheTimeout = config.cacheTimeout * 1000 * 60 * 60;
    var checkSum = btoa(config.movieFrom + ':' + config.movieTo);
    LocalStorageService.set(MOVIE_RATING_CACHE_EXPIRE, new Date().getTime() + cacheTimeout);
    LocalStorageService.set(MOVIE_RATING_CACHE_CHECK_SUM, checkSum);
  };
  
  movie.get = function() {
    return movies;
  };
  
  movie.addTrailer = function(item, trailer) {
    item.trailers = trailer.qualities;
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.addToFavorites = function(item) {
    favorites = favorites || [];
    favorites.push(item);
    LocalStorageService.set(MOVIE_FAVORITES, favorites);
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.removeFromFavorites = function(item) {
    for (var i = 0, ii = movies.length; i < ii; i++) {
      if (item.idIMDB === movies[i].idIMDB) {
        delete movies[i].favorite;
        break;
      }
    }
    for (var i = 0, ii = favorites.length; i < ii; i++) {
      if (item.idIMDB === favorites[i].idIMDB) {
        favorites.splice(i,1);
        break;
      }
    }
    
    LocalStorageService.set(MOVIE_FAVORITES, favorites);
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.getFavorites = function() {
    return favorites;
  };
  
  movie.toggleFavorites = function(item) {
    if (!item.favorite) {
      item.favorite = true;
      movie.addToFavorites(item);
    } else {
      delete item.favorite;
      movie.removeFromFavorites(item);
    }
  };
  
  movie.showDirectorInfo = function(director) {
    var link = config.directorPath + director.id ;
    window.open(link, '_system');
  };
  
  movie.getTrailer = function(item) {
    item.trailerLoading = true;
    var params = { idIMDB: item.idIMDB
                 , format: 'json'
                 , language: 'en-us'
                 , trailers: 1
                 };
    NetworkService.get('/idIMDB', params, function(result, response){
      delete item.trailerLoading;
      if (result) {
        movie.addTrailer(item, response.data.movies[0].trailer);
      }
    });
  };
  
  movie.getMoviesFromServer = function(callback) {
    callback = callback || function(){};
    var params = { start: config.movieFrom
                 , end: config.movieTo
                 , format: 'json'
                 , data: 1
                 };
    NetworkService.get('/top', params, function (result, response) {
      if (result) {
        var favorites = LocalStorageService.get(MOVIE_FAVORITES);
        try {
          var movies = response.data.movies;
          if (favorites) {
            for (var i = 0, ii = movies.length; i < ii; i++) {
              for (var n = 0, nn = favorites.length; n < nn; n++) {
                if (movies[i].idIMDB === favorites[n].idIMDB) {
                  movies[i] = favorites[n];
                }
              }
            }
          }
          
          callback(movies);
          movie.set(movies);
        } catch (error) {
          console.log(error);
        }
      }
    });
  };
  
  
  movie.showTrailer = function(trailer) {
    console.log('open');
    window.open(trailer.videoURL, '_blank');
  };
  
  movie.normalizeArray = function(array) {
    return array.join(', ');
  };
  
  movie.checkCacheTimeout = function() {
    var cacheTimeout = LocalStorageService.get(MOVIE_RATING_CACHE_EXPIRE)*1;
    var oldCheckSum = LocalStorageService.get(MOVIE_RATING_CACHE_CHECK_SUM);
    var newCheckSum = btoa(config.movieFrom + ':' + config.movieTo);
    if (!cacheTimeout) return false;
    console.log(((cacheTimeout - new Date().getTime()) / 1000 / 60) + 'm');
    if (new Date().getTime() < cacheTimeout && oldCheckSum === newCheckSum) {
      return true;
    }
    
    return false;
  };
  
}]);

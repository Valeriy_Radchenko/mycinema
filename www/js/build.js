var myCinema = angular.module('starter.controllers', []);

myCinema.service('config',[function() {
  var config = this;
  
  config.servicePath = 'http://www.myapifilms.com/imdb';
  config.directorPath = 'http://www.imdb.com/name/';
  
  config.CANCEL_TIMEOUT = 20000;
  config.MY_API_FILMS_TOKEN = 'fbafb7cf-61f3-4848-994c-3b08c8d88686';
  
  config.movieFrom = 1;
  config.movieTo = 20;
  
  config.cacheTimeout = 1; // in hours
  
  config.chartColors = [ '#F7464A'
                       , '#FFDA73' 
                       , '#218457'
                       , '#FF9D73'
                       , '#6C8DD5'
                       , '#BF5B30'
                       , '#FFAA00'
                       , '#BF9A30'
                       , '#70227E'
                       , '#949FB1'
                       , '#36D88E'
                       , '#BF8F30'
                       , '#FF4C00'
                       , '#5D016D'
                       , '#1142AA'
                       , '#D4CCC5'
                       , '#E2EAE9'
                       , '#FFD073'
                       , '#8F04A8'
                       , '#A66E00'
                       ];
  
}]);

myCinema.controller('FavoritesCtrl', ['$scope', 'NetworkService', 'MovieService', '$ionicModal', function($scope, NetworkService, MovieService, $ionicModal) {
  
  $scope.$on("$ionicView.beforeEnter", function(event, data) {
    $scope.noResultTitle = 'Please add some movie to favorite';
    $scope.movies = MovieService.getFavorites();
  });
  
  $scope.$on("$ionicView.afterEnter", function(event, data) {
    $scope.tabTitle = 'Favorite Movies';
  });

  $scope.getTrailer = MovieService.getTrailer;
  $scope.showDirectorInfo = MovieService.showDirectorInfo;
  $scope.toggleFavorites = MovieService.toggleFavorites;
  $scope.join = MovieService.normalizeArray;
  $scope.showTrailer = MovieService.showTrailer;

  
  
  
}]);
myCinema.controller('MoviesByDecadeCtrl', ['$scope', 'MovieService', 'config', 
  function($scope, MovieService, config) {
  
  var movies = [];
  var colors = config.chartColors;
  $scope.chartOptions =  { responsive: true
                         , animateRotate : false
                         , animateScale : false
                         };
  
  $scope.$on("$ionicView.beforeEnter", function(event, data){
    $scope.showChart = true;
    movies = MovieService.get();
    $scope.chartData = createChartData(movies);

    console.log($scope.myData);
  });
  
  $scope.$on("$ionicView.afterLeave", function(event, data){
    // Chart error "IndexSizeError" is prevented by this action, when opening modal window. 
    $scope.showChart = false; 
  });
  
  function createChartData(movies) {
    var dates = {};
    var result = [];
    var colorIndex = 0;
    
    for (var i = 0, ii = movies.length; i < ii; i++) {
      
      var movie = movies[i];
      var year =  movie.year.slice(0,-1) + '0';
      var data = {};
      
      if (dates[year]) {
        data = dates[year];
        data.value++;
        result[data.index] = data;
      } else {
        data.year = year;
        data = { value: 1
               , color: colors[colorIndex]
               , label: year +  ' - ' + (year*1 + 9)
               , year: year
               , index: result.length
               };
        colorIndex++;
        result.push(data);
      }
      
      dates[year] = data;
    }
    
    result.sort(function(a, b) {
      if (a.year < b.year) {
        return -1;
      }
      if (a.year > b.year) {
        return 1;
      }
      return 0;
    });
    
    return result;
  }
  
}]);
myCinema.controller('TopRatedCtrl', ['$scope', 'MovieService', function($scope, MovieService) {
  
  $scope.$on("$ionicView.beforeEnter", function(event, data) {
    $scope.noResultTitle = '';
    $scope.movies = MovieService.get();
    console.log($scope.movies);
    if (!$scope.movies || !MovieService.checkCacheTimeout()) {
      MovieService.getMoviesFromServer(function(movies) {
        $scope.movies = movies;
      });
    }
  });
  
  $scope.$on("$ionicView.afterEnter", function(event, data) {
    $scope.tabTitle = 'Top Rated Movies';
  });

  $scope.getTrailer = MovieService.getTrailer;
  $scope.showDirectorInfo = MovieService.showDirectorInfo;
  $scope.toggleFavorites = MovieService.toggleFavorites;
  $scope.join = MovieService.normalizeArray;
  $scope.showTrailer = MovieService.showTrailer;

  
  
 
  
}]);
myCinema.service('LocalStorageService',['$window', function($window) {
  var storage = this;
  
  var keyError = new Error('Key must be a string');
  
  storage.set = function(key, value) {
    if (typeof key !== 'string') throw keyError;
    
    if (typeof value !== 'string') {
      value = JSON.stringify(value);
    }
    
    key = 'myCinema-' + key;
    $window.localStorage.setItem(key, value);
  };
  
  storage.get = function(key) {
    if (typeof key !== 'string') throw keyError;
    
    key = 'myCinema-' + key;
    var result = $window.localStorage.getItem(key);
    if (!result) return false;
    
    try {
      result = JSON.parse(result);
    } catch (e) {}
    
    return result;
  };
  
  storage.remove = function(key) {
    if (typeof key !== 'string') throw keyError;
    
    key = 'myCinema-' + key;
    $window.localStorage.removeItem(key);
  };
  
}]);

myCinema.service('MovieService',['LocalStorageService', 'NetworkService', 'config',
  function(LocalStorageService, NetworkService, config) {
    
  var movie = this;
  var MOVIE_RATING = 'movie-rating';
  var MOVIE_RATING_CACHE_EXPIRE = 'movie-rating-cache-expire';
  var MOVIE_RATING_CACHE_CHECK_SUM = 'movie-rating-cache-check-sum';
  var MOVIE_FAVORITES = 'movie-favorites';
  var movies = LocalStorageService.get(MOVIE_RATING);
  var favorites = LocalStorageService.get(MOVIE_FAVORITES);
  var iframe = null;
  var iframeParams = {};
  
  movie.set = function(items) {
    movies = items;
    LocalStorageService.set(MOVIE_RATING, items);
    var cacheTimeout = config.cacheTimeout * 1000 * 60 * 60;
    var checkSum = btoa(config.movieFrom + ':' + config.movieTo);
    LocalStorageService.set(MOVIE_RATING_CACHE_EXPIRE, new Date().getTime() + cacheTimeout);
    LocalStorageService.set(MOVIE_RATING_CACHE_CHECK_SUM, checkSum);
  };
  
  movie.get = function() {
    return movies;
  };
  
  movie.addTrailer = function(item, trailer) {
    item.trailers = trailer.qualities;
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.addToFavorites = function(item) {
    favorites = favorites || [];
    favorites.push(item);
    LocalStorageService.set(MOVIE_FAVORITES, favorites);
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.removeFromFavorites = function(item) {
    for (var i = 0, ii = movies.length; i < ii; i++) {
      if (item.idIMDB === movies[i].idIMDB) {
        delete movies[i].favorite;
        break;
      }
    }
    for (var i = 0, ii = favorites.length; i < ii; i++) {
      if (item.idIMDB === favorites[i].idIMDB) {
        favorites.splice(i,1);
        break;
      }
    }
    
    LocalStorageService.set(MOVIE_FAVORITES, favorites);
    LocalStorageService.set(MOVIE_RATING, movies);
  };
  
  movie.getFavorites = function() {
    return favorites;
  };
  
  movie.toggleFavorites = function(item) {
    if (!item.favorite) {
      item.favorite = true;
      movie.addToFavorites(item);
    } else {
      delete item.favorite;
      movie.removeFromFavorites(item);
    }
  };
  
  movie.showDirectorInfo = function(director) {
    var link = config.directorPath + director.id ;
    window.open(link, '_system');
  };
  
  movie.getTrailer = function(item) {
    item.trailerLoading = true;
    var params = { idIMDB: item.idIMDB
                 , format: 'json'
                 , language: 'en-us'
                 , trailers: 1
                 };
    NetworkService.get('/idIMDB', params, function(result, response){
      delete item.trailerLoading;
      if (result) {
        movie.addTrailer(item, response.data.movies[0].trailer);
      }
    });
  };
  
  movie.getMoviesFromServer = function(callback) {
    callback = callback || function(){};
    var params = { start: config.movieFrom
                 , end: config.movieTo
                 , format: 'json'
                 , data: 1
                 };
    NetworkService.get('/top', params, function (result, response) {
      if (result) {
        var favorites = LocalStorageService.get(MOVIE_FAVORITES);
        try {
          var movies = response.data.movies;
          if (favorites) {
            for (var i = 0, ii = movies.length; i < ii; i++) {
              for (var n = 0, nn = favorites.length; n < nn; n++) {
                if (movies[i].idIMDB === favorites[n].idIMDB) {
                  movies[i] = favorites[n];
                }
              }
            }
          }
          
          callback(movies);
          movie.set(movies);
        } catch (error) {
          console.log(error);
        }
      }
    });
  };
  
  
  movie.showTrailer = function(trailer) {
    console.log('open');
    window.open(trailer.videoURL, '_blank');
  };
  
  movie.normalizeArray = function(array) {
    return array.join(', ');
  };
  
  movie.checkCacheTimeout = function() {
    var cacheTimeout = LocalStorageService.get(MOVIE_RATING_CACHE_EXPIRE)*1;
    var oldCheckSum = LocalStorageService.get(MOVIE_RATING_CACHE_CHECK_SUM);
    var newCheckSum = btoa(config.movieFrom + ':' + config.movieTo);
    if (!cacheTimeout) return false;
    console.log(((cacheTimeout - new Date().getTime()) / 1000 / 60) + 'm');
    if (new Date().getTime() < cacheTimeout && oldCheckSum === newCheckSum) {
      return true;
    }
    
    return false;
  };
  
}]);

myCinema.service('NetworkService',['$http', '$httpParamSerializer', 'config',
  function($http, $httpParamSerializer, config) {
  var $network = this;
  
  $network.servicePath = config.servicePath;
  
  if (navigator.userAgent.toLocaleLowerCase().indexOf('windows') > -1 
          || navigator.userAgent.toLowerCase().indexOf('macintosh') > -1) {
    $network.servicePath = 'http://localhost/myCinema/www/proxy.php';
  }

  var CANCEL_TIMEOUT = config.CANCEL_TIMEOUT,
      MY_API_FILMS_TOKEN = config.MY_API_FILMS_TOKEN; 
  
  $network.get = function(methodName, params, callback) {
    callback = callback || function(){};
    
    var path = $network.servicePath + methodName;
    
    if (params) {
      params.token = MY_API_FILMS_TOKEN;
      path = $network.servicePath + methodName + '?' + $httpParamSerializer(params);
    }
    
    $http({ 'method': 'get'
          , 'url': path
          , timeout: CANCEL_TIMEOUT
          })

            .success(function(result, status, headers) {
              callback(true, result);
            })

            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });
          
  };
  
  $network.getWithPromise = function(methodName, params) {
    var path = $network.servicePath + methodName;
    
    if (params) {
      params.token = MY_API_FILMS_TOKEN;
      path = $network.servicePath + methodName + '?' + $httpParamSerializer(params);
    }
    
    return $http({ 'method': 'get'
                 , 'url': path
                 , timeout: CANCEL_TIMEOUT
                 });
  };
  
  $network.post = function(methodName, params, callback) {
    
    callback = callback || function(){};
    
    params = params || {};

    var path = $network.servicePath + methodName;
    
    $http({ 'method': 'post'
          , 'url': path
          , 'data':  $httpParamSerializer(params)
          , 'headers' : {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
          , timeout: CANCEL_TIMEOUT
          })
            .success(function(result, status, headers) {
              callback(true, result);
            })
            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });

  };
  
  $network.put = function(methodName, params, callback, disableDefaultErrorHandler, noToken) {
    
    callback = callback || function(){};
    
    params = params || {};

    var path = $network.servicePath + methodName;

    $http({ 'method': 'put'
          , 'url': path
          , 'data':  params
          , timeout: CANCEL_TIMEOUT
          })
            .success(function(result, status, headers) {
              callback(true, result);
            })
            .error(function(data, status, headers, config, statusText) {
              callback(false,data,status);
            });

  };

}]);

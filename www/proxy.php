<?php

$url = 'http://www.myapifilms.com/imdb';

$str = str_replace("/myCinema/www/proxy.php",'',$_SERVER['REQUEST_URI']);
$url.=$str;

$inputHeaders = apache_request_headers();
$headers = array();
$allowHeaders = array ( 'Authorization'
                      , 'Content-Type'
                      , 'Accept'
                      //, 'Accept-Encoding'
                      , 'Accept-Language'
                      );
foreach ($inputHeaders as $key => $value){
  if (in_array($key, $allowHeaders)) {
    array_push($headers, $key.': '.$value); 
  }
}
$post = file_get_contents('php://input');


$ch = curl_init($url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
if ($_SERVER['REQUEST_METHOD'] == "POST") {
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
}
if ($_SERVER['REQUEST_METHOD'] == "DELETE") {
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
}
if ($_SERVER['REQUEST_METHOD'] == "PUT") {
  file_put_contents('put.txt',$post);
  $f = fopen('put.txt', 'r');
  curl_setopt($ch, CURLOPT_INFILE, $f);
  curl_setopt($ch, CURLOPT_INFILESIZE, filesize('put.txt'));
  curl_setopt($ch, CURLOPT_PUT, true);
}

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


$output = curl_exec($ch);
$code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
curl_close($ch);

header('X-PHP-Response-Code: '.$code, true, $code);
echo($output);

